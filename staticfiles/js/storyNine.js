$(document).ready(function(){
	setTimeout(function() {
	$('#preloader').fadeOut('slow');
	}, 1500);
	
	$.ajax({
		// url : "https://www.googleapis.com/books/v1/volumes?q=quilting",
		url : "/datajson",
		dataType : "json",
		success : function (datajson) {
			var allBooks = datajson.items;
			for (var i = 0; i < allBooks.length; i++) {
				var bookInfo = allBooks[i].volumeInfo;
				var penulis = "";
				
				for (var j = 0; j < bookInfo.authors.length; j++) {
				  penulis += bookInfo.authors[j];

				  if (j != bookInfo.authors.length - 1) {
					penulis += ", ";
				  }
				}
				
				$('#fillTheBooksIn').append("<tr>" +
				  "<td align='center' class='favorit'>☆</td>" +
				  "<th scope=\"row\">" + bookInfo.title + "</th>" +
				  "<td>" + penulis + "</td>" +
				  "<td>" + bookInfo.publisher + "</td>" +
				  "<td>" + bookInfo.categories[0] + "</td>" +
				"</tr>");
			}
		},
	});
	
	var counterfavorit = 0;

	updatefavorit();

	function updatefavorit() {
		$('#favTotal').text(counterfavorit);
	};

	//Favorit
	$(document).on('click', '.favorit', function(){
		if($(this).text() == "☆"){
			counterfavorit++;
			updatefavorit();
			$(this).text("★");
		}
		else{
			counterfavorit--;
			updatefavorit();
			$(this).text("☆");
		}
	});


});
