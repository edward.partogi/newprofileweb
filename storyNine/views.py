from django.shortcuts import render
from django.http import HttpResponse
import requests

# Create your views here.
def booklist(request) :
    return render(request, 'bukuBuks.html')

def datajson(request):
    data = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
    return HttpResponse(data, content_type='application/json')
