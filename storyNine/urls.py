from django.conf.urls import url
from .views import booklist, datajson

urlpatterns = [
    url('books', booklist, name='monthlyBooks'),
    url('datajson', datajson),
]
