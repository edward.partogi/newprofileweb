from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import booklist, datajson

# Create your tests here.

class Homepage_testcase(TestCase):

    def test_homepage_url_is_exist(self):
        response = Client().get('/books')
        self.assertEqual(response.status_code, 200)

    def test_using_booklist_func(self):
        found = resolve('/books')
        self.assertEqual(found.func, booklist)

    def test_datajson_url_is_exist(self):
        response = Client().get('/datajson')
        self.assertEqual(response.status_code, 200)

    def test_using_datajson_func(self):
        found = resolve('/datajson')
        self.assertEqual(found.func, datajson)
